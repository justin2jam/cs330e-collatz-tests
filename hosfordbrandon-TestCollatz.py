#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, cycle_length, compute

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read2(self):
        s = "10 1\n"
        i,j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 1)

    def test_read3(self):
        s = "2000 2500\n"
        i,j = collatz_read(s)
        self.assertEqual(i, 2000)
        self.assertEqual(j, 2500)


    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # ----
    # compute
    # ----

    def test_compute_1(self):
        v = compute(1, 10)
        self.assertEqual(v, 20)

    def test_compute_2(self):
        v = compute(100, 200)
        self.assertEqual(v, 125)

    def test_compute_3(self):
        v = compute(201, 210)
        self.assertEqual(v, 89)

    def test_compute_4(self):
        v = compute(900, 1000)
        self.assertEqual(v, 174)

    # ----
    # cycle_length
    # ----

    def test_cycle_1(self):
        v = cycle_length(60)
        self.assertEqual(v, 20)

    def test_cycle_2(self):
        v = cycle_length(7)
        self.assertEqual(v, 17)

    def test_cycle_3(self):
        v = cycle_length(999999)
        self.assertEqual(v, 259)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 20, 400, 144)
        self.assertEqual(w.getvalue(), "20 400 144\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 2, 3, 8)
        self.assertEqual(w.getvalue(), "2 3 8\n")
    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve2(self):
        r = StringIO("2 5\n30 40\n30 5\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2 5 8\n30 40 107\n30 5 112\n")

    def test_solve3(self):
        r = StringIO("200 434\n1 9000\n1000 2134\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "200 434 144\n1 9000 262\n1000 2134 182\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
